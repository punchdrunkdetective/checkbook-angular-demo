// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    './src/**/*.e2e-spec.ts'
  ],
  suites: {
    budgetFull: './src/budget/*.e2e-spec.ts',
    budgetList: './src/**/budget-list.e2e-spec.ts',
    budgetCreate: './src/**/budget-create.e2e-spec.ts',
    budgetView: './src/**/budget-view.e2e-spec.ts',
    budgetUpdate: './src/**/budget-update.e2e-spec.ts',
    allocationFull: './src/allocation/*.e2e-spec.ts',
    allocationList: './src/**/allocation-list.e2e-spec.ts',
    allocationCreate: './src/**/allocation-create.e2e-spec.ts',
    allocationView: './src/**/allocation-view.e2e-spec.ts',
    allocationUpdate: './src/**/allocation-update.e2e-spec.ts',
    expenseFull: './src/expense/*.e2e-spec.ts',
    expenseList: './src/**/expense-list.e2e-spec.ts',
    expenseCreate: './src/**/expense-create.e2e-spec.ts',
    expenseUpdate: './src/**/expense-update.e2e-spec.ts'
  },
  capabilities: {
    'browserName': 'chrome'
  },
  directConnect: true,
  baseUrl: 'http://localhost:4444/wd/hub',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};