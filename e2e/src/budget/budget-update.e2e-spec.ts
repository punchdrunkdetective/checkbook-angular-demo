import { BudgetUpdatePage } from './budget-update.po';

describe('Budget Update', () => {

  const budget = 'budget 1'; //TODO: figure out how to centralize mock data
  let updatePage = new BudgetUpdatePage(budget);

  describe('Page loading', () => {

    beforeEach(() => updatePage.navigateTo());  

    it('Should be loaded', () => expect(updatePage.getTitle()).toMatch('Budget Update'));
    it('Should have correct url', () => expect(updatePage.getCurrentUrl()).toMatch(`budgets/update`));    

    it('Should load name in form field', () => expect(updatePage.form.name).toMatch('budget 1'));
    it('Should load start date in form field', () => expect(updatePage.form.startDate).toMatch('2018-12-01'));
    it('Should load end date in form field', () => expect(updatePage.form.endDate).toMatch('2018-12-08'));
    it('Should load amount in form field', () => expect(updatePage.form.amount).toBe(200.021));
    it('Should have Save enabled by default', () => expect(updatePage.form.canSave()).toBe(true));

  });

  describe('Clicking clear', () => {

    beforeEach(() => 
      updatePage
        .navigateTo()
        .then(() => updatePage.form.clear())
    );

    it('Should clear name in form field', () => expect(updatePage.form.name).toMatch(''));
    it('Should clear start date in form field', () => expect(updatePage.form.startDate).toMatch(''));
    it('Should clear end date in form field', () => expect(updatePage.form.endDate).toMatch(''));
    it('Should clear amount in form field', () => expect(updatePage.form.amount).toBe(0));
    it('Should have Save disabled', () => expect(updatePage.form.canSave()).toBe(false));

  });

  describe('Clearing name', () => {

    beforeEach(() => 
      updatePage
        .navigateTo()
        .then(() => updatePage.form.enterName(''))
    );
    
    it('Should have Save disabled', () => expect(updatePage.form.canSave()).toBe(false));
    
  });

  describe('Clearing start date', () => {

    beforeEach(() => 
      updatePage
        .navigateTo()
        .then(() => updatePage.form.clearStartDate())
    );
    
    //TODO: test fails. issue with how dat validation is triggered.
    it('Should have Save disabled', () => expect(updatePage.form.canSave()).toBe(false));
    
  });

  describe('Clearing end date', () => {

    beforeEach(() => 
      updatePage
        .navigateTo()
        .then(() => updatePage.form.clearEndDate())
    );
    
    //TODO: test fails. issue with how dat validation is triggered.
    it('Should have Save disabled', () => expect(updatePage.form.canSave()).toBe(false));
    
  });

  describe('Clearing amount', () => {

    beforeEach(() => 
      updatePage
        .navigateTo()
        .then(() => updatePage.form.clearAmount())
    );
    
    it('Should have Save disabled', () => expect(updatePage.form.canSave()).toBe(false));

  })

});
