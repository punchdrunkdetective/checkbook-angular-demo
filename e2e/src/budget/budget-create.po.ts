import { browser, by, element } from 'protractor';
import { Page } from '../page.po';
import { BudgetForm } from './budget-form.po';

export class BudgetCreatePage extends Page {

  private _form: BudgetForm;
  get form() { return this._form; }

  constructor() {
    super();
    this._form = new BudgetForm();
  }

  navigateTo() { return browser.get('/budgets/create'); }
  getTitle() { return element(by.id('pageTitle')).getText(); }

}
