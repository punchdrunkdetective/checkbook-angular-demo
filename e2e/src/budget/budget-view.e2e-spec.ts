import { browser } from 'protractor';
import { BudgetViewPage } from './budget-view.po';

describe('Budget View', () => {

  const budget = 'budget 1';
  let viewPage = new BudgetViewPage(budget);

  describe('Page loading', () => {

    beforeEach(() => viewPage.navigateTo() );  

    it('Should be loaded', () => expect(viewPage.getTitle()).toMatch('Details'));
    it('Should have correct url', () => expect(browser.getCurrentUrl()).toMatch(`budgets/`));

  });

});
