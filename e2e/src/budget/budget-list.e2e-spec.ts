import { browser } from 'protractor';
import { BudgetListPage } from './budget-list.po';
import { BudgetCreatePage } from './budget-create.po';
import { BudgetViewPage } from './budget-view.po';
import { BudgetUpdatePage } from './budget-update.po';

describe('Budget List', () => {

  let listPage = new BudgetListPage();

  describe('Clicking add', () => {

    let createPage: BudgetCreatePage;

    beforeEach(() => 
      listPage
        .navigateTo()
        .then(() => listPage.clickAdd())
        .then(() => createPage = new BudgetCreatePage())
    );  

    it('Should navigate to the add page', () => expect(createPage.getTitle()).toMatch('Create Budget'));
    it('Should update the url', () => expect(browser.getCurrentUrl()).toMatch('budgets/create'));

  });

  describe('Clicking details', () => {

    const budget = 'budget 1';    
    let viewPage: BudgetViewPage;

    beforeEach(() => 
      listPage
        .navigateTo()
        .then(() => listPage.clickDetails(budget))
        .then(() => viewPage = new BudgetViewPage(budget))
    )

    it('Should navigate to the details page', () => expect(viewPage.getTitle()).toMatch('Details'));
    it('Should update the url', () => expect(browser.getCurrentUrl()).toMatch('budgets/'));

  });

  describe('Clicking update', () => {

    const budget = 'budget 1';
    let updatePage: BudgetUpdatePage;

    beforeEach(() => 
      listPage
        .navigateTo()
        .then(() => listPage.clickUpdate(budget))
        .then(() => updatePage = new BudgetUpdatePage(budget))
    );

    it('Should navigate to the update page', () => expect(updatePage.getTitle()).toMatch('Budget Update'));
    it('Should update the url', () => expect(browser.getCurrentUrl()).toMatch('budgets/update'));

  });

});
