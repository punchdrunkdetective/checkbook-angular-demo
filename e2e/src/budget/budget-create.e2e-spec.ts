import { BudgetCreatePage } from './budget-create.po';

describe('Budget Create', () => {

  let createPage = new BudgetCreatePage();

  describe('Page loading', () => {

    beforeEach(() => createPage.navigateTo());  

    it('Should be loaded', () => expect(createPage.getTitle()).toMatch('Create Budget'));
    it('Should have correct url', () => expect(createPage.getCurrentUrl()).toMatch(`budgets/create`));

    it('Should show empty name in form field', () => expect(createPage.form.name).toMatch(''));
    it('Should show empty start date in form field', () => expect(createPage.form.startDate).toMatch(''));
    it('Should show empty end date in form field', () => expect(createPage.form.endDate).toMatch(''));
    it('Should show empty amount in form field', () => expect(createPage.form.amount).toBe(0));
    it('Should have Save disabled by default', () => expect(createPage.form.canSave()).toBe(false));

  });

  describe('Entering valid data', () => {

    beforeEach(() => 
      createPage
        .navigateTo()
        .then(() =>
          createPage.form.enterBudget({
            name: 'test budget',
            startDate: '08-07-1990',
            endDate: '09-03-1991',
            amount: 50
          }))
    );

    it('Should have Save enabled', () => expect(createPage.form.canSave()).toBe(true));

  });

  describe('Entering partial data', () => {

    describe('Missing name', () => {

      beforeEach(() => 
        createPage
          .navigateTo()
          .then(() =>
            createPage.form.enterBudget({
              name: '',
              startDate: '08-07-1990',
              endDate: '09-03-1991',
              amount: 50
            }))
      );

      it('Should have Save disabled', () => expect(createPage.form.canSave()).toBe(false));

    });

    describe('Missing start date', () => {

      beforeEach(() => 
        createPage
          .navigateTo()
          .then(() =>
            createPage.form.enterBudget({
              name: 'test budget',
              startDate: '',
              endDate: '09-03-1991',
              amount: 50
            }))
      );

      it('Should have Save disabled', () => expect(createPage.form.canSave()).toBe(false));

    });

    describe('Missing end date', () => {

      beforeEach(() => 
        createPage
          .navigateTo()
          .then(() =>
            createPage.form.enterBudget({
              name: 'test budget',
              startDate: '08-07-1990',
              endDate: '',
              amount: 50
            }))
      );

      it('Should have Save disabled', () => expect(createPage.form.canSave()).toBe(false));

    });

    describe('Missing amount', () => {

      beforeEach(() => 
        createPage
          .navigateTo()
          .then(() =>
            createPage.form.enterBudget({
              name: 'test budget',
              startDate: '08-07-1990',
              endDate: '09-03-1991',
              amount: 50
            }))
          .then(() => createPage.form.clearAmount())
      );

      it('Should have Save disabled', () => expect(createPage.form.canSave()).toBe(false));

    });

  });

  describe('Clicking clear', () => {

    beforeEach(() =>
      createPage
        .navigateTo()
        .then(() => 
          createPage.form.enterBudget({
            name: 'test budget',
            startDate: '08-07-1990',
            endDate: '09-03-1991',
            amount: 50
          }))
        .then(() => createPage.form.clear())
    );

    it('Should clear name in form field', () => expect(createPage.form.name).toMatch(''));
    it('Should clear start date in form field', () => expect(createPage.form.startDate).toMatch(''));
    it('Should clear end date in form field', () => expect(createPage.form.endDate).toMatch(''));
    it('Should clear amount in form field', () => expect(createPage.form.amount).toBe(0));
    it('Should have Save disabled', () => expect(createPage.form.canSave()).toBe(false));

  });

});
