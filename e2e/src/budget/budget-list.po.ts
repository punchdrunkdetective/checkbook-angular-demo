import { browser, by, element } from 'protractor';

export class BudgetListPage {

  navigateTo() {
    return browser.get('/budgets');
  }

  clickAdd() {
    let addBtn = element(by.xpath('//button[text()="Add"]'));
    return addBtn
      .click() // navigates to create page
      .then(() => browser.waitForAngular());
  }

  clickDetails(budgetName: string) {
    let detailsLnk = element(by.xpath(`//tbody//*[text()='${budgetName}']`));
    return detailsLnk
      .click() // navigates to create page
      .then(() => browser.waitForAngular());
  }

  clickUpdate(budgetName: string) {
    let updateBtn = element(by.xpath(`//tbody//tr[.//*[text()='${budgetName}']]//*[text()='Update']`));
    return updateBtn
      .click() // navigates to create page
      .then(() => browser.waitForAngular());
  }

}
