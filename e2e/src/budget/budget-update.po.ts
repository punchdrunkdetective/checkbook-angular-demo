import { browser, by, element } from 'protractor';
import { Page } from '../page.po';
import { BudgetForm } from './budget-form.po';

export class BudgetUpdatePage extends Page {

  private _form: BudgetForm;
  get form() { return this._form; }

  constructor(private budgetName: string) { 
    super();
    this._form = new BudgetForm();
  }

  navigateTo() { return browser.get(`/budgets/update/${this.budgetName}`); }
  getTitle() { return element(by.id('pageTitle')).getText(); } 

}
