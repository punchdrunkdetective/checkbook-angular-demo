import { browser, by, element } from 'protractor';

export class BudgetViewPage {

  constructor(private budgetName: string) { }

  navigateTo() { return browser.get(`/budgets/${this.budgetName}`); }
  getTitle() { return element(by.id('pageTitle')).getText(); }

}
