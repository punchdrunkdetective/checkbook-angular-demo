import { by, element } from 'protractor';
import { Form } from '../form.po';

export class BudgetForm extends Form {

  get name() { return this.getNameInput().getAttribute('value'); }
  get startDate() { return this.getStartDateInput().getAttribute('value'); }
  get endDate() { return this.getEndDateInput().getAttribute('value'); }
  get amount() { return this.getAmountInput().getAttribute('value').then((amtStr) => amtStr ? parseFloat(amtStr) : 0); }

  enterName(val: string) { return this.enterInput(this.getNameInput(), val); } 
  clearName() { return this.clearInput(this.getNameInput()); }

  enterStartDate(val: string) { return this.enterDateInput(this.getStartDateInput(), val); }
  clearStartDate() { return this.clearDateInput(this.getStartDateInput()); }
  
  enterEndDate(val: string) { return this.enterDateInput(this.getEndDateInput(), val); }
  clearEndDate() { return this.clearDateInput(this.getEndDateInput()); }

  enterAmount(val: number) { return this.enterInput(this.getAmountInput(), val); }
  clearAmount() { return this.clearInput(this.getAmountInput()); }

  enterBudget(budget: any) {
    return this.enterName(budget.name)
    .then(() => this.enterStartDate(budget.startDate))
    .then(() => this.enterEndDate(budget.endDate))
    .then(() => this.enterAmount(budget.amount));
  }

  canSave() { return this.getSaveBtn().isEnabled(); }
  clear() { return this.getClearBtn().click(); }

  private getNameInput() { return element(by.name('name')); }
  private getStartDateInput() { return element(by.name('startDate')); }
  private getEndDateInput() { return element(by.name('endDate')); }
  private getAmountInput() { return element(by.name('amount')); }

  private getSaveBtn() { return element(by.xpath('//button[text()="Save"]')); }
  private getClearBtn() { return element(by.xpath('//button[text()="Clear"]')); }

}
