import { by, element, Key, ElementFinder } from 'protractor';

export class Form {

    protected clearInput(input: ElementFinder) {
        return input
        .sendKeys('') // forces cursor to input of input
        .then(() => input.getAttribute('value'))
        .then((currVal) => currVal.split('') )
        .then((chars) => chars.map(() => input.sendKeys(Key.BACK_SPACE)))
        .then((backKeys) => Promise.all(backKeys))
        .then(() => this.clickAway());
    }
    
    protected enterInput(input: ElementFinder, val: (string | number)) {
        return this.clearInput(input)
        .then(() => input.sendKeys(val))
        .then(() => this.clickAway());
    }

    protected clearDateInput(input: ElementFinder) {
        return input
        .sendKeys(Key.BACK_SPACE)
        .then(() => input.sendKeys(Key.TAB))
        .then(() => input.sendKeys(Key.BACK_SPACE))
        .then(() => input.sendKeys(Key.TAB))
        .then(() => input.sendKeys(Key.BACK_SPACE))
        .then(() => this.clickAway());
    }

    protected enterDateInput(input: ElementFinder, val) {
        return this.clearDateInput(input)
        .then(() => input.sendKeys(val))
        .then(() => this.clickAway());
    }

    protected clickAway() { return element(by.xpath('//form')).click(); } // triggers on blur events to fire
}