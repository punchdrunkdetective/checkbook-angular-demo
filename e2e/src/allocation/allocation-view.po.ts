import { browser, by, element } from 'protractor';

export class AllocationViewPage {

    constructor(private budgetName: string, private allocationName: string) { }

    navigateTo() { return browser.get(`allocations/${this.budgetName}/${this.allocationName}`) }
    getTitle() { return element(by.id('pageTitle')).getText(); }

}