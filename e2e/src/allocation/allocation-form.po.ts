import { by, element } from 'protractor';
import { Form } from '../form.po';

export class AllocationForm extends Form {

  get name() { return this.getNameInput().getAttribute('value'); }
  get amount() { return this.getAmountInput().getAttribute('value').then((amtStr) => amtStr ? parseFloat(amtStr) : 0); }

  enterName(name: string) { return this.enterInput(this.getNameInput(), name); }
  clearName() { return this.clearInput(this.getNameInput()); }

  enterAmount(amount: number) { return this.enterInput(this.getAmountInput(), amount); }
  clearAmount() { return this.clearInput(this.getAmountInput()); }

  enterAllocation(allocation: any) {
    return this.enterName(allocation.name)
      .then(() => this.enterAmount(allocation.amount))
  }

  canSave() { return this.getSaveBtn().isEnabled(); }
  clear() { return this.getClearBtn().click(); }

  private getNameInput() { return element(by.name('name')); }
  private getAmountInput() { return element(by.name('amount')); }

  private getSaveBtn() { return element(by.xpath('//button[text()="Save"]')); }
  private getClearBtn() { return element(by.xpath('//button[text()="Clear"]')); }

}
