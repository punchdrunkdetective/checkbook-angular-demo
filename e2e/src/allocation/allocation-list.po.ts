import { browser, by, element } from 'protractor';

export class AllocationListPage {

  constructor(private budgetName: string) { }

  navigateTo() {
    return browser.get(`/allocations/${this.budgetName}`);
  }

  clickAdd() {
    let addBtn = element(by.xpath('//button[text()="Add"]'));
    return addBtn
      .click() // navigates to create page
      .then(() => browser.waitForAngular())
  }

  clickDetails(allocationName: string) {
    let detailsLnk = element(by.xpath(`//tbody//*[text()='${allocationName}']`));
    return detailsLnk
      .click() // navigates to details page
      .then(() => browser.waitForAngular())
  }

  clickUpdate(allocationName: string) {
    let updateBtn = element(by.xpath(`//tbody//tr[.//*[text()='${allocationName}']]//*[text()='Update']`));
    return updateBtn
      .click() // navigates to update page
      .then(() => browser.waitForAngular())
  }

}
