import { AllocationCreatePage } from './allocation-create.po';

describe('Allocation Create', () => {

  const budget = 'budget 1';
  let createPage = new AllocationCreatePage(budget);

  describe('Page loading', () => {

    beforeEach(() => createPage.navigateTo() );  

    it('Should be loaded', () => expect(createPage.getTitle()).toMatch('Create Allocation'));
    it('Should have correct url', () => expect(createPage.getCurrentUrl()).toMatch('allocations/create'));

    it('Should show empty name', () => expect(createPage.form.name).toMatch(''));
    it('Should show empty amount', () => expect(createPage.form.amount).toBe(0));
    it('Should have Save disabled by default', () => expect(createPage.form.canSave()).toBe(false));  

  });

  describe('Entering valid data', () => {

    beforeEach(() => 
      createPage
        .navigateTo()
        .then(() => 
          createPage.form.enterAllocation({
            name: 'test budget',
            amount: 50
          }))
    );

    it('Should have Save enabled', () => expect(createPage.form.canSave()).toBe(true)); 

  });

  describe('Entering partial data', () => {

    describe('Missing name', () => {

      beforeEach(() => 
        createPage
          .navigateTo()
          .then(() => 
            createPage.form.enterAllocation({
              name: 'test budget',
              amount: 50
            }))
          .then(() => createPage.form.clearName()));

      it('Should have Save disabled', () => expect(createPage.form.canSave()).toBe(false) );

    });

    describe('Missing amount', () => {

      beforeEach(() => 
        createPage
          .navigateTo()
          .then(() => 
            createPage.form.enterAllocation({
              name: 'test budget',
              amount: 50
            }))
          .then(() => createPage.form.clearAmount()));

      it('Should have Save disabled', () => expect(createPage.form.canSave()).toBe(false) );

    });

  });

  describe('Clicking clear', () => {

    beforeEach(() => 
      createPage
        .navigateTo()
        .then(() => 
          createPage.form.enterAllocation({
            name: 'test budget',
            amount: 50
          }))
        .then(() => createPage.form.clear()));

    it('Should show empty name', () => expect(createPage.form.name).toMatch(''));
    it('Should show empty amount', () => expect(createPage.form.amount).toBe(0));
    it('Should have Save disabled by default', () => expect(createPage.form.canSave()).toBe(false));  

  });

});
