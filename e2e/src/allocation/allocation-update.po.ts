import { browser, by, element } from 'protractor';
import { Page } from '../page.po';
import { AllocationForm } from './allocation-form.po';

export class AllocationUpdatePage extends Page {

    private _form: AllocationForm;
    get form() { return this._form }

    constructor(private budgetName: string, private allocationName: string) { 
        super();
        this._form = new AllocationForm();
    }

    navigateTo() { return browser.get(`allocations/update/${this.budgetName}/${this.allocationName}`); }
    getTitle() { return element(by.id('pageTitle')).getText(); }

}