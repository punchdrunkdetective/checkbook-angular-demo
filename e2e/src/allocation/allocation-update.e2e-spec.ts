import { AllocationUpdatePage } from './allocation-update.po';

describe('Allocation Update', () => {

  const budget = 'budget 1';
  const allocation = 'free spending 1';
  let updatePage = new AllocationUpdatePage(budget, allocation);

  describe('Page loading', () => {

    beforeEach(() => updatePage.navigateTo() );  

    it('Should be loaded', () => expect(updatePage.getTitle()).toMatch('Update Allocation'));
    it('Should have correct url', () => expect(updatePage.getCurrentUrl()).toMatch(`allocations/update`));

    it('Should load name in form field', () => expect(updatePage.form.name).toMatch('free spending 1'));
    it('Should load amount in form field', () => expect(updatePage.form.amount).toBe(50.00));
    it('Should have Save enabled by default', () => expect(updatePage.form.canSave()).toBe(true));

  });

  describe('Clicking clear', () => {

    beforeEach(() => 
      updatePage
        .navigateTo()
        .then(() => updatePage.form.clear())
    );

    it('Should clear name in form field', () => expect(updatePage.form.name).toMatch(''));
    it('Should clear amount in form field', () => expect(updatePage.form.amount).toBe(0));
    it('Should have Save disabled', () => expect(updatePage.form.canSave()).toBe(false));

  });

  describe('Clearing name', () => {

    beforeEach(() => 
      updatePage
        .navigateTo()
        .then(() => updatePage.form.clearName())
    );

    it('Should have Save disabled', () => expect(updatePage.form.canSave()).toBe(false));

  });

  describe('Clearing amount', () => {

    beforeEach(() => 
      updatePage
        .navigateTo()
        .then(() => updatePage.form.clearAmount())
    );

    it('Should have Save disabled', () => expect(updatePage.form.canSave()).toBe(false));

  });

});
