import { browser } from 'protractor';
import { AllocationListPage } from './allocation-list.po';
import { AllocationCreatePage } from './allocation-create.po';
import { AllocationViewPage } from './allocation-view.po';
import { AllocationUpdatePage } from './allocation-update.po';

describe('Allocation List', () => {

  const budget = 'budget 1'; 
  let listPage = new AllocationListPage(budget);

  describe('Clicking add', () => {

    let createPage: AllocationCreatePage;

    beforeEach(() => 
      listPage
        .navigateTo()
        .then(() => listPage.clickAdd())
        .then(() => createPage = new AllocationCreatePage(budget))
    );  

    it('Should navigate to the add page', () => expect(createPage.getTitle()).toMatch('Create Allocation'));
    it('Should update the url', () => expect(browser.getCurrentUrl()).toMatch('allocations/create'));

  });

  describe('Clicking details', () => {

    const allocation = 'free spending 1';   
    let viewPage: AllocationViewPage;

    beforeEach(() => 
      listPage
        .navigateTo()
        .then(() => listPage.clickDetails(allocation))
        .then(() => viewPage = new AllocationViewPage(budget, allocation))
    );

    it('Should navigate to the details page', () => expect(viewPage.getTitle()).toMatch('Details'));
    it('Should update the url', () => expect(browser.getCurrentUrl()).toMatch('allocations/'));

  });

  describe('Clicking update', () => {

    const allocation = 'free spending 1';
    let updatePage: AllocationUpdatePage;

    beforeEach(() =>
      listPage
        .navigateTo()
        .then(() => listPage.clickUpdate(allocation))
        .then(() => updatePage = new AllocationUpdatePage(budget, allocation))
    );

    it('Should navigate to the update page', () => expect(updatePage.getTitle()).toMatch('Update Allocation'));
    it('Should update the url', () => expect(browser.getCurrentUrl()).toMatch('allocations/update'));

  });

});
