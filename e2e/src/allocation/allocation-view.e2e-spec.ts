import { browser } from 'protractor';
import { AllocationViewPage } from './allocation-view.po';

describe('Allocation Details', () => {

  const budget = 'budget 1';
  const allocation = 'free spending 1';
  let viewPage = new AllocationViewPage(budget, allocation);

  describe('Page loading', () => {

    beforeEach(() => 
      viewPage.navigateTo()
    );  

    it('Should be loaded', () => expect(viewPage.getTitle()).toMatch('Details'));
    it('Should have correct url', () => expect(browser.getCurrentUrl()).toMatch(`allocations/`));

  });

});
