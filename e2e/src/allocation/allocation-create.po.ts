import { browser, by, element } from 'protractor';
import { Page } from '../page.po';
import { AllocationForm } from './allocation-form.po';

export class AllocationCreatePage extends Page {

    private _form: AllocationForm;
    get form() { return this._form }

    constructor(private budgetname: string) { 
        super();
        this._form = new AllocationForm();
    }

    navigateTo() { return browser.get(`allocations/create/${this.budgetname}`); }
    getTitle() { return element(by.id('pageTitle')).getText(); }

}