import { browser } from 'protractor';

export class Page {
    getCurrentUrl() { return browser.getCurrentUrl(); }
}