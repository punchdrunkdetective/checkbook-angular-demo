import { ExpenseCreatePage } from './expense-create.po';

describe('Expense Create', () => {

  const budget = 'budget 1';
  const allocation = 'free spending 1';
  let createPage = new ExpenseCreatePage(budget, allocation);

  describe('Page loading', () => {

    beforeEach(() => createPage.navigateTo());  

    it('Should be loaded', () => expect(createPage.getTitle()).toMatch('Expense Create'));
    it('Should have correct url', () => expect(createPage.getCurrentUrl()).toMatch(`expenses/create`));

    it('Should show empty entry date in form field', () => expect(createPage.form.entryDate).toMatch(''));
    it('Shoudd show empty description in form field', () => expect(createPage.form.description).toMatch(''));
    it('Should show empty amount in form field', () => expect(createPage.form.amount).toBe(0.00));
    it('Should have Save disabled by default', () => expect(createPage.form.canSave()).toBe(false));

  });

  describe('Entering valid data', () => {

    beforeEach(() => 
      createPage
        .navigateTo()
        .then(() => 
          createPage.form.enterExpense({
            entryDate: '09-03-1990',
            description: 'test description',
            amount: 1.00
          })));

    it('Should have Save enabled', () => expect(createPage.form.canSave()).toBe(true));

  });

  describe('Entering partial data', () => {

    describe('Missing entry date', () => {

      beforeEach(() => 
        createPage
          .navigateTo()
          .then(() => 
            createPage.form.enterExpense({
              entryDate: '',
              description: 'test description',
              amount: 1.00
            })));

      it('Should have Save disabled by default', () => expect(createPage.form.canSave()).toBe(false));

    });

    describe('Missing description', () => {

      beforeEach(() => 
        createPage
          .navigateTo()
          .then(() => 
            createPage.form.enterExpense({
              entryDate: '09-03-1990',
              description: '',
              amount: 1.00
            })));

      it('Should have Save disabled by default', () => expect(createPage.form.canSave()).toBe(false));

    });

    describe('Missing amount', () => {

      beforeEach(() => 
        createPage
          .navigateTo()
          .then(() => 
            createPage.form.enterExpense({
              entryDate: '09-03-1990',
              description: 'test description',
              amount: 1.00
            }))
          .then(() => createPage.form.clearAmount()));

      it('Should have Save disabled by default', () => expect(createPage.form.canSave()).toBe(false));

    });

  });

  describe('Clicking clear', () => {

    beforeEach(() => 
      createPage
        .navigateTo()
        .then(() => 
          createPage.form.enterExpense({
            entryDate: '09-03-1990',
            description: 'test description',
            amount: 1.00
          }))
        .then(() => createPage.form.clear()));

    it('Should show empty entry date in form field', () => expect(createPage.form.entryDate).toMatch(''));
    it('Shoudd show empty description in form field', () => expect(createPage.form.description).toMatch(''));
    it('Should show empty amount in form field', () => expect(createPage.form.amount).toBe(0.00));
    it('Should have Save disabled by default', () => expect(createPage.form.canSave()).toBe(false));

  });

});
