import { by, element } from 'protractor';
import { Form } from '../form.po';

export class ExpenseForm extends Form {

  get entryDate() { return this.getEntryDateInput().getAttribute('value'); }
  get description() { return this.getDescriptionInput().getAttribute('value'); }
  get amount() { return this.getAmountInput().getAttribute('value').then((amtStr) => amtStr ? parseFloat(amtStr) : 0) }

  enterEntryDate(entryDate: string) { return this.enterDateInput(this.getEntryDateInput(), entryDate); }
  clearEntryDate() { return this.clearDateInput(this.getEntryDateInput()); }

  enterDescription(description: string) { return this.enterInput(this.getDescriptionInput(), description); }
  clearDescription() { return this.clearInput(this.getDescriptionInput()); }

  enterAmount(amount: number) { return this.enterInput(this.getAmountInput(), amount); }
  clearAmount() { return this.clearInput(this.getAmountInput()); }

  enterExpense(expense: any) {
    return this.enterEntryDate(expense.entryDate)
      .then(() => this.enterDescription(expense.description))
      .then(() => this.enterAmount(expense.amount));
  }

  canSave() { return this.getSaveBtn().isEnabled(); }
  clear() { return this.getClearBtn().click() }

  private getEntryDateInput() { return element(by.name('entryDate')); }
  private getDescriptionInput() { return element(by.name('description')); }
  private getAmountInput() { return element(by.name('amount')); }

  private getSaveBtn() { return element(by.xpath('//button[text()="Save"]')); }
  private getClearBtn() { return element(by.xpath('//button[text()="Clear"]')); }

}
