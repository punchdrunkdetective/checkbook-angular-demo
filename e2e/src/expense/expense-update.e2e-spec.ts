import { ExpenseUpdatePage } from './expense-update.po';

describe('Expense Update', () => {

  const budget = 'budget 1';
  const allocation = 'free spending 1';
  const expense = 1;
  let updatePage = new ExpenseUpdatePage(budget, allocation, expense);

  describe('Page loading', () => {

    beforeEach(() => updatePage.navigateTo() );  

    it('Should be loaded', () => expect(updatePage.getTitle()).toMatch('Expense Update'));
    it('Should have correct url', () => expect(updatePage.getCurrentUrl()).toMatch(`expenses/update`));

    it('Should load entry date in form field', () => expect(updatePage.form.entryDate).toMatch('2018-12-03'));
    it('Shoudd load description in form field', () => expect(updatePage.form.description).toMatch('budget 1 expense 1'));
    it('Should load amount in form field', () => expect(updatePage.form.amount).toBe(10.00));
    it('Should have Save enabled by default', () => expect(updatePage.form.canSave()).toBe(true));

  });

  describe('Clicking clear', () => {

    beforeEach(() => 
      updatePage
        .navigateTo()
        .then(() => updatePage.form.clear())
    );

    it('Should clear entry date in form field', () => expect(updatePage.form.entryDate).toMatch(''));
    it('Shoudd clear description in form field', () => expect(updatePage.form.description).toMatch(''));
    it('Should clear amount in form field', () => expect(updatePage.form.amount).toBe(0));
    it('Should have Save disabled', () => expect(updatePage.form.canSave()).toBe(false));

  });

  describe('Clearing entry date', () => {

    beforeEach(() => 
      updatePage
        .navigateTo()
        .then(() => updatePage.form.clearEntryDate())
    );

    //TODO: test fails. issue with how dat validation is triggered.
    it('Should have Save disabled', () => expect(updatePage.form.canSave()).toBe(false));

  });

  describe('Clearing description', () => {

    beforeEach(() => 
      updatePage
        .navigateTo()
        .then(() => updatePage.form.clearDescription())
    );

    it('Should have Save disabled', () => expect(updatePage.form.canSave()).toBe(false));

  });

  describe('Clearing amount', () => {

    beforeEach(() => 
      updatePage
        .navigateTo()
        .then(() => updatePage.form.clearAmount())
    );

    it('Should have Save disabled', () => expect(updatePage.form.canSave()).toBe(false));

  });

});
