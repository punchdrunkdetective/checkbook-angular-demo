import { browser, by, element } from 'protractor';
import { Page } from '../page.po';
import { ExpenseForm } from './expense-form.po';

export class ExpenseCreatePage extends Page {

  private _form: ExpenseForm;
  get form() { return this._form; }

  constructor(private budateName: string, private allocationName: string) { 
    super();
    this._form = new ExpenseForm();
  }

  navigateTo() { return browser.get(`expenses/create/${this.budateName}/${this.allocationName}`); }
  getTitle() { return element(by.id('pageTitle')).getText(); }

}
