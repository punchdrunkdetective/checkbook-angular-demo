import { browser, by, element } from 'protractor';

export class ExpenseListPage {

  constructor(private budgetName: string, private allocationName: string) { } 

  navigateTo() {
    return browser.get(`/expenses/${this.budgetName}/${this.allocationName}`);
  }

  clickAdd() {
    let addBtn = element(by.xpath('//button[text()="Add"]'));
    return addBtn
      .click() // navigates to create page
      .then(() => browser.waitForAngular());
  }

  clickUpdate(idx: number) {
    let updateBtn = element(by.xpath(`//tbody//tr[${idx}]//*[text()='Update']`));
    return updateBtn
      .click() // navigates to update page
      .then(() => browser.waitForAngular());
  }

}
