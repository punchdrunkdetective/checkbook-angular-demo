import { browser } from 'protractor';
import { ExpenseListPage } from './expense-list.po';
import { ExpenseCreatePage } from './expense-create.po';
import { ExpenseUpdatePage } from './expense-update.po';

describe('Expense List', () => {

  const budget = 'budget 1';
  const allocation = 'free spending 1';
  let listPage = new ExpenseListPage(budget, allocation);

  describe('Clicking add', () => {

    let createPage: ExpenseCreatePage;

    beforeEach(() => 
      listPage
        .navigateTo()
        .then(() => listPage.clickAdd())
        .then(() => createPage = new ExpenseCreatePage(budget, allocation))
    );  

    it('Should navigate to the add page', () => expect(createPage.getTitle()).toMatch('Expense Create'));
    it('Should update the url', () => expect(browser.getCurrentUrl()).toMatch('expenses/create'));

  });

  describe('Clicking update', () => {

    const expense = 1;
    let updatePage: ExpenseUpdatePage;

    beforeEach(() => 
      listPage
        .navigateTo()
        .then(() => listPage.clickUpdate(expense)) //NOtE: it is coincidence that id = idx here
        .then(() => updatePage = new ExpenseUpdatePage(budget, allocation, expense))
    );

    it('Should navigate to the update page', () => expect(updatePage.getTitle()).toMatch('Expense Update'));
    it('Should update the url', () => expect(browser.getCurrentUrl()).toMatch('expenses/update'));

  });

});
