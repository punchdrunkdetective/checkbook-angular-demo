import { browser, by, element } from 'protractor';
import { Page } from '../page.po';
import { ExpenseForm } from './expense-form.po';

export class ExpenseUpdatePage extends Page {

  private _form: ExpenseForm;
  get form() { return this._form; }

  constructor(private budgetName: string, private allocationName: string, private expenseId: number) {
    super();
    this._form = new ExpenseForm();
   }

  navigateTo() { return browser.get(`expenses/update/${this.budgetName}/${this.allocationName}/${this.expenseId}`); }
  getTitle() { return element(by.id('pageTitle')).getText(); }

}
