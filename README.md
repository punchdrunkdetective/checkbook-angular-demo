# Checkbook Angular Demo 


## What is this repository for? 

This is a demo app used to explore different technologies and patterns within the Angular framework. It is an application designed to model a checkbook, allowing the user to allocate money in a budget and record transactions made within that budget.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.2.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).


## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Contribution guidelines 

TBD


## Who do I talk to? 

Me! at punchdrunkdetective@gmail.com

