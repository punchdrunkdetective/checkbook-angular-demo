import { Expense } from './expense';

export class Allocation {

    name: string;
    amount: number;
    expenses: Expense[];

    constructor(data) {

        this.name = data.name;
        this.amount = data.amount;

        this.expenses = data.expenses ? data.expenses : [];

    }

}