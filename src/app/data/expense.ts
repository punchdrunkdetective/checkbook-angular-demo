export class Expense {

    id: number;
    entryDate: string;
    description: string;
    amount: number;

    constructor(data) {
        this.id = data.id;
        this.entryDate = data.entryDate;
        this.description = data.description;
        this.amount = data.amount;
    }

}