import { Injectable } from '@angular/core';

import { Budget } from './budget';
import { Allocation } from './allocation';
import { Expense } from './expense';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    getBudgets(): Budget[] {
        return [
            new Budget({
              name: 'budget 1',
              startDate: new Date('12/1/2018'),
              endDate: new Date('12/8/2018'),
              amount: 200.021,
              allocations: [
                new Allocation({
                  name: "expenses 1",
                  amount: 150.00,
                  expenses: [
                    new Expense({
                      id: 1,
                      entryDate: new Date('12/3/2018'), 
                      description: 'budget 1 expense 1',
                      amount: 10.00
                    }),
                    new Expense({
                      id: 2,
                      entryDate: new Date('12/4/2018'), 
                      description: 'budget 1 expense 2',
                      amount: 20.00
                    }),
                    new Expense({
                      id: 3,
                      entryDate: new Date('12/5/2018'), 
                      description: 'budget 1 expense 3',
                      amount: 30.00
                    }),
                    new Expense({
                      id: 4,
                      entryDate: new Date('12/6/2018'), 
                      description: 'budget 1 expense 4',
                      amount: 40.00
                    })
                  ]
                }),
                new Allocation({
                  name: "free spending 1",
                  amount: 50.00,
                  expenses: [
                    new Expense({
                      id: 1,
                      entryDate: new Date('12/3/2018'), 
                      description: 'budget 1 expense 1',
                      amount: 10.00
                    }),
                    new Expense({
                      id: 2,
                      entryDate: new Date('12/4/2018'), 
                      description: 'budget 1 expense 2',
                      amount: 20.00
                    }),
                    new Expense({
                      id: 3,
                      entryDate: new Date('12/5/2018'), 
                      description: 'budget 1 expense 3',
                      amount: 30.00
                    }),
                    new Expense({
                      id: 4,
                      entryDate: new Date('12/6/2018'), 
                      description: 'budget 1 expense 4',
                      amount: 40.00
                    })
                  ]
                })
              ]
            }),        
            new Budget({
              name: 'budget 2',
              startDate: new Date('12/9/2018'),
              endDate: new Date('12/15/2018'),
              amount: 200.021,
              allocations: [
                new Allocation({
                  name: "expenses 2",
                  amount: 150.00,
                  expenses: [
                    new Expense({
                      id: 1,
                      entryDate: new Date('12/11/2018'), 
                      description: 'budget 2 expense 1',
                      amount: 10.00
                    }),
                    new Expense({
                      id: 2,
                      entryDate: new Date('12/12/2018'), 
                      description: 'budget 2 expense 2',
                      amount: 20.00
                    }),
                    new Expense({
                      id: 3,
                      entryDate: new Date('12/13/2018'), 
                      description: 'budget 2 expense 3',
                      amount: 30.00
                    }),
                    new Expense({
                      id: 4,
                      entryDate: new Date('12/14/2018'), 
                      description: 'budget 2 expense 4',
                      amount: 40.00
                    })
                  ]
                }),
                new Allocation({
                  name: "free spending 2",
                  amount: 50.00,
                  expenses: [
                    new Expense({
                      id: 1,
                      entryDate: new Date('12/11/2018'), 
                      description: 'budget 2 expense 1',
                      amount: 10.00
                    }),
                    new Expense({
                      id: 2,
                      entryDate: new Date('12/12/2018'), 
                      description: 'budget 2 expense 2',
                      amount: 20.00
                    }),
                    new Expense({
                      id: 3,
                      entryDate: new Date('12/13/2018'), 
                      description: 'budget 2 expense 3',
                      amount: 30.00
                    }),
                    new Expense({
                      id: 4,
                      entryDate: new Date('12/14/2018'), 
                      description: 'budget 2 expense 4',
                      amount: 40.00
                    })
                  ]
                })
              ]
            }),        
            new Budget({
              name: 'budget 3',
              startDate: new Date('12/16/2018'),
              endDate: new Date('12/22/2018'),
              amount: 200.021,
              allocations: [
                new Allocation({
                  name: "expenses 3",
                  amount: 150.00,
                  expenses: [
                    new Expense({
                      id: 1,
                      entryDate: new Date('12/17/2018'), 
                      description: 'budget 3 expense 1',
                      amount: 10.00
                    }),
                    new Expense({
                      id: 2,
                      entryDate: new Date('12/18/2018'), 
                      description: 'budget 3 expense 2',
                      amount: 20.00
                    }),
                    new Expense({
                      id: 3,
                      entryDate: new Date('12/19/2018'), 
                      description: 'budget 3 expense 3',
                      amount: 30.00
                    }),
                    new Expense({
                      id: 4,
                      entryDate: new Date('12/20/2018'), 
                      description: 'budget 3 expense 4',
                      amount: 40.00
                    })
                  ]
                }),
                new Allocation({
                  name: "free spending 3",
                  amount: 50.00,
                  expenses: [
                    new Expense({
                      id: 1,
                      entryDate: new Date('12/17/2018'), 
                      description: 'budget 3 expense 1',
                      amount: 10.00
                    }),
                    new Expense({
                      id: 2,
                      entryDate: new Date('12/18/2018'), 
                      description: 'budget 3 expense 2',
                      amount: 20.00
                    }),
                    new Expense({
                      id: 3,
                      entryDate: new Date('12/19/2018'), 
                      description: 'budget 3 expense 3',
                      amount: 30.00
                    }),
                    new Expense({
                      id: 4,
                      entryDate: new Date('12/20/2018'), 
                      description: 'budget 3 expense 4',
                      amount: 40.00
                    })
                  ]
                })
              ]
            }),        
            new Budget({
              name: 'budget 4',
              startDate: new Date('12/23/2018'),
              endDate: new Date('12/31/2018'),
              amount: 200.021,
              allocations: [
                new Allocation({
                  name: "expenses 4",
                  amount: 150.00,
                  expenses: [
                    new Expense({
                      id: 1,
                      entryDate: new Date('12/24/2018'), 
                      description: 'budget 4 expense 1',
                      amount: 10.00
                    }),
                    new Expense({
                      id: 2,
                      entryDate: new Date('12/25/2018'), 
                      description: 'budget 4 expense 2',
                      amount: 20.00
                    }),
                    new Expense({
                      id: 3,
                      entryDate: new Date('12/26/2018'), 
                      description: 'budget 4 expense 3',
                      amount: 30.00
                    }),
                    new Expense({
                      id: 4,
                      entryDate: new Date('12/27/2018'), 
                      description: 'budget 4 expense 4',
                      amount: 40.00
                    })
                  ]
                }),
                new Allocation({
                  name: "free spending 4",
                  amount: 50.00,
                  expenses: [
                    new Expense({
                      id: 1,
                      entryDate: new Date('12/24/2018'), 
                      description: 'budget 4 expense 1',
                      amount: 10.00
                    }),
                    new Expense({
                      id: 2,
                      entryDate: new Date('12/25/2018'), 
                      description: 'budget 4 expense 2',
                      amount: 20.00
                    }),
                    new Expense({
                      id: 3,
                      entryDate: new Date('12/26/2018'), 
                      description: 'budget 4 expense 3',
                      amount: 30.00
                    }),
                    new Expense({
                      id: 4,
                      entryDate: new Date('12/27/2018'), 
                      description: 'budget 4 expense 4',
                      amount: 40.00
                    })
                  ]
                })
              ]
            })
          ];
    }

    getBudget(name: string): Budget { return this.getBudgets().find((elm) => elm.name == name); }

    getAllocation(budgetName: string, allocationName: string): Allocation { return this.getBudget(budgetName).allocations.find((elm) => elm.name == allocationName); }

    getExpense(budgetName: string, allocationName: string, expenseId: number) { return this.getAllocation(budgetName, allocationName).expenses.find((elm) => elm.id == expenseId) }

}