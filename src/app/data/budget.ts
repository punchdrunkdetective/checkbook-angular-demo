import { Allocation } from './allocation';

export class Budget {

    name: string;
    startDate: string;
    endDate: string;
    amount: number;
    allocations: Allocation[];

    constructor(data) {

        this.name = data.name;
        this.startDate = data.startDate;
        this.endDate = data.endDate;
        this.amount = data.amount;

        this.allocations = data.allocations ? data.allocations : [];
        
    }

}