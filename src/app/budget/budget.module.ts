import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { BudgetRoutingModule } from './budget-routing.module';
import { NavigationModule } from '../navigation/navigation.module';
import { AllocationModule } from '../allocation/allocation.module';

import { BudgetCreateComponent } from './budget-create.component';
import { BudgetListComponent } from './budget-list.component';
import { BudgetViewComponent } from './budget-view.component';
import { BudgetUpdateComponent } from './budget-update.component';
import { BudgetFormComponent } from './budget-form/budget-form.component';

@NgModule({
  declarations: [
    BudgetCreateComponent,
    BudgetListComponent,
    BudgetViewComponent,
    BudgetUpdateComponent,
    BudgetFormComponent
  ],
  imports: [
    SharedModule,
    AllocationModule,
    BudgetRoutingModule,
    NavigationModule
  ]
})
export class BudgetModule { }
