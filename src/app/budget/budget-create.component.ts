import { Component } from '@angular/core';

@Component({
  templateUrl: './budget-create.component.html',
  styleUrls: ['./budget-create.component.css']
})
export class BudgetCreateComponent {
  title: string = 'Create Budget';  
}
