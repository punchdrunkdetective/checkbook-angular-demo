import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BudgetCreateComponent } from './budget-create.component';
import { BudgetListComponent } from './budget-list.component';
import { BudgetViewComponent } from './budget-view.component';
import { BudgetUpdateComponent } from './budget-update.component';

const routes: Routes = [
  { path: 'budgets', component: BudgetListComponent },
  { path: 'budgets/create', component: BudgetCreateComponent },
  { path: 'budgets/:budgetId', component: BudgetViewComponent },
  { path: 'budgets/update/:budgetId', component: BudgetUpdateComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BudgetRoutingModule { }
