import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DataService } from '../data/data.service';
import { Budget } from '../data/budget';

@Component({
  templateUrl: './budget-update.component.html',
  styleUrls: ['./budget-update.component.css']
})
export class BudgetUpdateComponent implements OnInit {

  title: string = 'Budget Update';
  budget: Budget = null;

  constructor(private route: ActivatedRoute, private dataService: DataService) { }

  submitted: boolean = false;
  success: boolean = false;
  save(): void { 
    this.submitted = true; 
    this.success = !this.success;
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('budgetId');
    this.budget = this.dataService.getBudget(id);
  }

}
