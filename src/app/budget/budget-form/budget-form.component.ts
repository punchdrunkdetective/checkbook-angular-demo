import { Component, Input } from '@angular/core';

@Component({
  selector: 'budget-form',
  templateUrl: './budget-form.component.html',
  styleUrls: ['./budget-form.component.css']
})
export class BudgetFormComponent {

  @Input()
  name: string;

  @Input()
  startDate: Date;

  @Input()
  endDate: Date;

  @Input()
  amount: number;

  submitted: boolean = false;
  success: boolean = false;
  save(): void { 

    this.submitted = true; 
    this.success = !this.success;

    console.log({
      name: this.name,
      startDate: this.startDate,
      endDate: this.endDate,
      amount: this.amount
    });

  }

  getDate(dateStr): Date {
    let dateParts = dateStr.split('-');
    return new Date(dateParts[0], dateParts[1] - 1, dateParts[2], 0, 0, 0, 0);
  }

  getAmount(amt): number {
    return amt >= 0 ? amt : -1 * amt
  }

}
