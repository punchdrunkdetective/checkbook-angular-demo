import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Budget } from '../data/budget';
import { DataService } from '../data/data.service';

@Component({
  templateUrl: './budget-list.component.html',
  styleUrls: ['./budget-list.component.css']
})
export class BudgetListComponent implements OnInit {

  constructor(private router: Router, private data: DataService) { }

  title: string = 'Budget List';
  budgetList: Budget[] = [];

  ngOnInit(): void {
    this.budgetList = this.data.getBudgets();
  }

  add(): void { 
    this.router.navigate(['/budgets/create']); 
  }

  update(name): void {
    this.router.navigate(['/budgets/update', name]);
  }

  delete(name): void {
      alert(`delete called for ${name}`);
  }

}
