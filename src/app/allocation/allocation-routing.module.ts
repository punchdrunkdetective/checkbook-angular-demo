import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllocationCreateComponent } from './allocation-create.component';
import { AllocationListComponent } from './allocation-list.component';
import { AllocationViewComponent } from './allocation-view.component';
import { AllocationUpdateComponent } from './allocation-update.component';

const routes: Routes = [
  { path: 'allocations/create/:budgetId', component: AllocationCreateComponent },
  { path: 'allocations/:budgetId', component: AllocationListComponent },
  { path: 'allocations/:budgetId/:allocationId', component: AllocationViewComponent },
  { path: 'allocations/update/:budgetId/:allocationId', component: AllocationUpdateComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllocationRoutingModule { }
