import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './allocation-create.component.html',
  styleUrls: ['./allocation-create.component.css']
})
export class AllocationCreateComponent implements OnInit {

  title: string = 'Create Allocation';
  budgetId: string = '';

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.budgetId = this.route.snapshot.paramMap.get('budgetId');
  }

}
