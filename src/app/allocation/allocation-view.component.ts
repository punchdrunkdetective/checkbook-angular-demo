import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DataService } from '../data/data.service';
import { Allocation } from '../data/allocation';

@Component({
  templateUrl: './allocation-view.component.html',
  styleUrls: ['./allocation-view.component.css']
})
export class AllocationViewComponent implements OnInit {

  budgetId: string = '';
  allocationId: string = '';
  title: string = 'Details';
  allocation: Allocation = null;

  constructor(private route: ActivatedRoute, private dataService: DataService) { }

  ngOnInit() {
    this.budgetId = this.route.snapshot.paramMap.get('budgetId');
    this.allocationId = this.route.snapshot.paramMap.get('allocationId');
    this.allocation = this.dataService.getAllocation(this.budgetId, this.allocationId);
  }

}
