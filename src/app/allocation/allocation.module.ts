import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { AllocationRoutingModule } from './allocation-routing.module';
import { NavigationModule } from '../navigation/navigation.module';
import { ExpenseModule } from '../expense/expense.module';

import { AllocationCreateComponent } from './allocation-create.component';
import { AllocationListComponent } from './allocation-list.component';
import { AllocationViewComponent } from './allocation-view.component';
import { AllocationUpdateComponent } from './allocation-update.component';
import { AllocationFormComponent } from './allocation-form/allocation-form.component';

@NgModule({
  declarations: [
    AllocationCreateComponent,
    AllocationListComponent,
    AllocationViewComponent,
    AllocationUpdateComponent,
    AllocationFormComponent
  ],
  imports: [
    SharedModule,
    ExpenseModule,
    AllocationRoutingModule,
    NavigationModule    
  ],
  exports: [
    AllocationListComponent
  ]
})
export class AllocationModule { }
