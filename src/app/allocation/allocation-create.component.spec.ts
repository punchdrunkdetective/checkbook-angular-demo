import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllocationCreateComponent } from './allocation-create.component';

describe('AllocationCreateComponent', () => {
  let component: AllocationCreateComponent;
  let fixture: ComponentFixture<AllocationCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllocationCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllocationCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
