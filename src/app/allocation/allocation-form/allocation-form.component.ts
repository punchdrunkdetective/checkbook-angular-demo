import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'allocation-form',
  templateUrl: './allocation-form.component.html',
  styleUrls: ['./allocation-form.component.css']
})
export class AllocationFormComponent implements OnInit {

  @Input()
  name: string;

  @Input()
  amount: number;

  constructor() { }

  ngOnInit() { }

  submitted: boolean = false;
  success: boolean = false;
  save(): void { 

    this.submitted = true; 
    this.success = !this.success;

    console.log({
      name: this.name,
      amount: this.amount
    })

  }

  getAmount(amt): number {
    return amt >= 0 ? amt : -1 * amt
  }

}
