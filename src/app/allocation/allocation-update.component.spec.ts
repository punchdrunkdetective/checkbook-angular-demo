import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllocationUpdateComponent } from './allocation-update.component';

describe('AllocationUpdateComponent', () => {
  let component: AllocationUpdateComponent;
  let fixture: ComponentFixture<AllocationUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllocationUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllocationUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
