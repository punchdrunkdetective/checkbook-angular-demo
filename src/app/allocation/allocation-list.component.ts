import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DataService } from '../data/data.service';
import { Allocation } from '../data/allocation';

@Component({
  selector: 'allocation-list',
  templateUrl: './allocation-list.component.html',
  styleUrls: ['./allocation-list.component.css']
})
export class AllocationListComponent implements OnInit {

  budgetId: string = '';
  title: string = "Allocations";
  allocations: Allocation[] = null;

  constructor(private route: ActivatedRoute, private router: Router, private dataService: DataService) { }

  ngOnInit() {
    this.budgetId = this.route.snapshot.paramMap.get('budgetId');
    const budget = this.dataService.getBudget(this.budgetId);
    this.allocations = budget.allocations;
  }

  add(): void { this.router.navigate(['/allocations/create', this.budgetId]); }

  update(name: string): void { this.router.navigate(['/allocations/update', this.budgetId, name]) }

  delete(name: string): void { alert(`delete called on ${name}` )}

}
