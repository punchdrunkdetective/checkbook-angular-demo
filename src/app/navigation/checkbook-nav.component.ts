import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'checkbook-nav',
  templateUrl: './checkbook-nav.component.html',
  styleUrls: ['./checkbook-nav.component.css']
})
export class CheckbookNavComponent implements OnInit {

  title: string = 'Checkbook Angular Demo';

  budgetId: string = '';
  allocationId: string = '';
  expenseId: number = -1;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.budgetId = this.route.snapshot.paramMap.get('budgetId');
    this.allocationId = this.route.snapshot.paramMap.get('allocationId');
    this.expenseId = +this.route.snapshot.paramMap.get('expenseId');
  }

}
