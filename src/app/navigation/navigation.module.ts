import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CheckbookNavComponent } from './checkbook-nav.component';

@NgModule({
  declarations: [
    CheckbookNavComponent
  ],
  imports: [    
    CommonModule,
    RouterModule
  ],
  exports: [
      CheckbookNavComponent
  ]
})
export class NavigationModule { }
