import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { ExpenseRoutingModule } from './expense-routing.module';
import { NavigationModule } from '../navigation/navigation.module';

import { ExpenseCreateComponent } from './expense-create.component';
import { ExpenseListComponent } from './expense-list.component';
import { ExpenseUpdateComponent } from './expense-update.component';
import { ExpenseFormComponent } from './expense-form/expense-form.component';

@NgModule({
  declarations: [
    ExpenseCreateComponent,
    ExpenseListComponent,
    ExpenseUpdateComponent,
    ExpenseFormComponent    
  ],
  imports: [
    SharedModule,
    ExpenseRoutingModule,
    NavigationModule
  ],
  exports: [
      ExpenseListComponent
  ]
})
export class ExpenseModule { }
