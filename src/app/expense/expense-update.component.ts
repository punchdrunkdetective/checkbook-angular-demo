import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Expense } from '../data/expense';
import { DataService } from '../data/data.service';

@Component({
  templateUrl: './expense-update.component.html',
  styleUrls: ['./expense-update.component.css']
})
export class ExpenseUpdateComponent implements OnInit {

  budgetId: string = '';
  allocationId: string = '';
  expenseId: number = -1;
  title: string = "Expense Update";
  expense: Expense = null;

  constructor(private route: ActivatedRoute, private dataService: DataService) { }

  ngOnInit() {

    this.budgetId = this.route.snapshot.paramMap.get('budgetId');
    this.allocationId = this.route.snapshot.paramMap.get('allocationId');
    this.expenseId = +this.route.snapshot.paramMap.get('expenseId');

    this.expense = this.dataService.getExpense(this.budgetId, this.allocationId, this.expenseId);

  }

}
