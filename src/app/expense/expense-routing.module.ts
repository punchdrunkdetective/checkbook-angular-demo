import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExpenseCreateComponent } from './expense-create.component';
import { ExpenseListComponent } from './expense-list.component';
import { ExpenseUpdateComponent } from './expense-update.component';

const routes: Routes = [
    { path: 'expenses/:budgetId/:allocationId', component: ExpenseListComponent },
    { path: 'expenses/create/:budgetId/:allocationId', component: ExpenseCreateComponent },
    { path: 'expenses/update/:budgetId/:allocationId/:expenseId', component: ExpenseUpdateComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExpenseRoutingModule { }
