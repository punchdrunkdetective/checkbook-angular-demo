import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Expense } from '../data/expense';
import { DataService } from '../data/data.service';

@Component({
  selector: 'expense-list',
  templateUrl: './expense-list.component.html',
  styleUrls: ['./expense-list.component.css']
})
export class ExpenseListComponent implements OnInit {

  budgeteId: string = '';
  allocationId: string = '';
  title: string = 'Expenses';
  expenses: Expense[] = null;

  constructor(private route: ActivatedRoute, private router: Router, private dataService: DataService) { }

  ngOnInit() {

    this.budgeteId = this.route.snapshot.paramMap.get('budgetId');
    this.allocationId = this.route.snapshot.paramMap.get('allocationId');

    let allocation = this.dataService.getAllocation(this.budgeteId, this.allocationId);
    this.expenses = allocation.expenses;

  }

  add(): void { this.router.navigate(['/expenses/create', this.budgeteId, this.allocationId]); }

  update(expense: Expense) { this.router.navigate(['/expenses/update', this.budgeteId, this.allocationId, expense.id]) }

  delete(expense: Expense) { alert(`delete called on ${expense.description}`)}

}
