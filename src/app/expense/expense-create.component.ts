import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './expense-create.component.html',
  styleUrls: ['./expense-create.component.css']
})
export class ExpenseCreateComponent implements OnInit {

  budgetId: string = '';
  allocationId: string = '';
  title: string = 'Expense Create';

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.budgetId = this.route.snapshot.paramMap.get('budgetId');
    this.allocationId = this.route.snapshot.paramMap.get('allocationId');
  }

}
