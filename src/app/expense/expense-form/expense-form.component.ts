import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'expense-form',
  templateUrl: './expense-form.component.html',
  styleUrls: ['./expense-form.component.css']
})
export class ExpenseFormComponent implements OnInit {

  @Input()
  entryDate: Date;

  @Input()
  description: string;

  @Input()
  amount: number;

  constructor() { }

  ngOnInit() { }
  
  submitted: boolean = false;
  success: boolean = false;
  save(): void { 

    this.submitted = true; 
    this.success = !this.success;
    
    console.log({
      entryDate: this.entryDate,
      description: this.description, 
      amount: this.amount
    })
    
  }

  getDate(dateStr): Date {
    let dateParts = dateStr.split('-');
    return new Date(dateParts[0], dateParts[1] - 1, dateParts[2], 0, 0, 0, 0);
  }

  getAmount(amt): number {
    return amt >= 0 ? amt : -1 * amt
  }

}
